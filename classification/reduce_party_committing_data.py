# !/usr/bin/env python3
# Tomer Gabay (s2726769)
# Bachelor Thesis
# 05-06-17
# Python3
# Linux command: Python3 reduce_party_committing_tweets.py

import os
import random

def main():
	reduce_lines(os.getcwd()+'/txt_files/party_committing_tweets.txt')
	

def reduce_lines(f):
	f = open(f,'r')
	gl = []
	pvv =[]
	d66 =[]
	cda = []
	for line in f:
		line2 = line.split('	')
		party = line2[2].strip()
		if party == 'GroenLinks':
			gl.append(line)
		elif party == 'D66':
			d66.append(line)
		elif party == 'CDA':
			cda.append(line)
		elif party == 'PVV':
			pvv.append(line)
	gl = gl[:1000]
	pvv = pvv[:1000]
	d66 = d66[:1000]
	cda = cda[:1000]
	total = gl + pvv + d66 + cda
	random.shuffle(total)
	f_out = open(os.getcwd()+'/txt_files/4000_party_committing_tweets.txt','w')
	for line in total:
		f_out.write(line)
	f_out.close()
if __name__ == "__main__":
	main()
