# !/usr/bin/env python
# Tomer Gabay (s2726769)
# Bachelor Thesis
# 05-06-17
# Python2
# Linux command: Python collect_data.py

import os
import re
import tweepy
import random
import cPickle as pickle

from time import sleep
from collections import defaultdict

	
def main():
	tweet_file_list = get_tweet_files()
	find_tweets_with_stem(tweet_file_list)
	parties_dict = create_parties_dict(os.getcwd()+'/txt_files/party_keywords.txt')
	users_vote_dict = find_party_committing_tweets(os.getcwd()+'/txt_files/tweets_with_stem.txt', parties_dict)
	twitter_API(users_vote_dict)
	
def get_tweet_files():
	
	"""Generates list with the names of the files containing tweets"""
	
	tweet_file_list = []
	for i in range(1,17):
		if i < 10:
			tweet_file = os.getcwd()+"/unfiltered_tweet_files/tweets17_030{}.txt".format(str(i))
		else:
			tweet_file = os.getcwd()+"/unfiltered_tweet_files/tweets17_03{}.txt".format(str(i))
		tweet_file_list.append(tweet_file)
	return tweet_file_list

def find_tweets_with_stem(tweet_file_list):
	
	"""Writes user and tweet of all tweets with 'stem' in it to 'tweets_with_stem.txt' """
	
	dic = defaultdict(list)	# key = user, value = tweet with stem
	f = open(os.getcwd()+'/txt_files/tweets_with_stem.txt','w')
	for tweets_file in tweet_file_list:
		print("Filtering tweets of "+tweets_file.split('/')[-1])
		tweets_file = open(tweets_file, "r")
		for line in tweets_file:
			user, tweet = line.split('	') 
			if 'stem' in tweet:
				if tweet not in dic[user]:	# prevents same tweet being assigned to same user more than once
					f.write(line)
					dic[user].append(tweet)
		tweets_file.close()
	f.close()
	
def create_parties_dict(f_parties):
	
	"""Creates dictionairy with key = *name of party*, value = *different spellings of party name, name of party leader,
	important twitter accounts officially related with party*"""
	
	parties_dict = {}
	with open(f_parties,'r') as f_parties:
		for line in f_parties: 
			line = line.strip()
			if line != "":
				party_name,key_associations = line.split('|')
				parties_dict[party_name] = key_associations.split(',')	# List of dif. spellings of party name, party leader, offical related accounts
	return parties_dict	
	
def find_party_committing_tweets(tweetfile, parties_dict):
	
	""" Tries to find users who are committed to one single party by analyzing tweets. Creates a file (party_committing_tweets.txt) with for each line: 
	*unique user* *party committing tweet* *committed party. """
	
	users_vote_dict = {} # key = user, value = committed party
	out_party_committing_tweets = open(os.getcwd()+'/txt_files/party_committing_tweets.txt','w')	# file with for each line *unique user* *party committing tweet* *committed party*
	
	with open(tweetfile,'r') as data_file:
		for line in data_file:
			line = line.rstrip().lower()
			assigned_to_party = False
			OK = True
			user,tweet = line.split('	')
			hashtags = []
			hashtags = re.findall(r"#(\w+)",tweet)
			call_to_vote = [vote_for for vote_for in hashtags if 'stem' in vote_for]
			for hashtag in call_to_vote:	#searches for hastags like #stempvv #ikstemcda
				for party,associations in parties_dict.items():
					call_to_vote_string = "stem{}".format(party.lower())
					if call_to_vote_string in hashtag:
						if assigned_to_party == False:
							assigned_to_party = True
							cur_party = party
						else:
							if cur_party != party:	# if one tweet seems to be committed to more than one party, the tweet isn't considered valid
								OK == False
					for association in associations:
						if "stem {}".format(association) in tweet or "stem op {}".format(association) in tweet or "stem voor {}".format(association) in tweet or "heb gestemd op {}".format(association) in tweet or "heb gestemd voor {}".format(association) in tweet or "heb op {} gestemd".format(association) in tweet or "heb voor gestemd {}".format(association) in tweet:
							if assigned_to_party == False:
								assigned_to_party = True
								cur_party = party
							else:
								if cur_party != party: # if one tweet seems committed to more than one party, the tweet isn't considered valid
									OK == False
			
			if assigned_to_party == True and OK == True:
				if user in users_vote_dict:
					if users_vote_dict[user] != party:	# if one user seems committed to more than one party, the user isn't considered valid
						OK = False
				else:
					if OK == True:
						party_committing_tweet = "	".join([user,tweet,cur_party,'\n'])
						out_party_committing_tweets.write(party_committing_tweet)
						users_vote_dict[user] = cur_party
	return users_vote_dict
	
def twitter_API(users_vote_dict):
	
	"""Collects up to 200 users and 200 friends per party committing user, and dumps dictionairy with key = username, value = [[tweets],[friends],party] to raw_data.p """
	
	userlist = [user for user,party in users_vote_dict.items()]
	auth = tweepy.OAuthHandler('EBV66Z9768ycqDDQkhxrFHTt8', 'FJz4zuFoELvVoay0hNH4ISRG2uPh9FCH6tLfCnOynaEY0n1vbl')
	auth.set_access_token('252053149-8zo7c5bTlIlOTjOlsTASOZ7yfGY40Oy154L6ZDzR', '1gNWtr0CDOM5ID6to1ApJdIZrFScTaZcsN66axdgZUmKS')
	api = tweepy.API(auth)
	try:
		f = open(os.getcwd()+'/pickle_files/data_raw.p','rb')
		user_data = pickle.load(f)
		f.close()
	except:
		user_data = {}
	i = 0 # iterates through list
	j = 0 # counts how many new users have been found
	go = True
	while True:
		try:
			if userlist[i] not in user_data:	#checks if data of user has already been downloaded 
				if users_vote_dict[userlist[i]] == "D66" or users_vote_dict[userlist[i]] == "CDA" or users_vote_dict[userlist[i]] == "GroenLinks" or users_vote_dict[userlist[i]] == "PVV":
					user = api.get_user(userlist[i])
					friends = user.friends(count = 200)
					friend_list = []
					for friend in friends:
						friend_list.append(friend.screen_name.encode('utf-8'))
					tweets = api.user_timeline(screen_name=userlist[i], include_retweets = True, count = 200)
					tweet_list = []
					for tweet in tweets:
						temp_tweet = tweet.text.strip().encode('utf-8')
						tweet_list.append(temp_tweet)	
					user_data[userlist[i]] = [tweet_list, friend_list,users_vote_dict[userlist[i]]]
					i += 1
					j += 1
				else:
					i += 1
			else:
				i += 1
		except tweepy.RateLimitError:
			print('sleeping')
			print(i,j)
			f = open(os.getcwd()+'/pickle_files/data_raw.p', 'wb')
			pickle.dump(user_data, f)
			f.close()
			sleep(15*60)
			
		except tweepy.TweepError, e:
			print(e)
			i += 1
			


if __name__ == "__main__":
	main()
