# !/usr/bin/env python3
# Tomer Gabay (s2726769)
# Bachelor Thesis
# 05-06-17
# Python3
# Linux command: Python3 classify_data.py all dev 			for classifying all tweets on the development set
# Linux command: Python3 classify_data.py all total 		for classifying all tweets on the total set
# Linux command: Python3 classify_data.py personal dev		for classifying personal tweets on the development set
# Linux command: Python3 classify_data.py personal total	for classifying personal tweets on the total set
# Linux command: Python3 classify_data.py retweets dev 		for classifying retweets tweets on the development set
# Linux command: Python3 classify_data.py retweets total 	for classifying retweets tweets on the total set

import os
import re
import sys
import nltk
import pickle
import sklearn

from sklearn import svm
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score, classification_report
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

def main(argv):
	data_file, data_set = choose_data_type_and_set(argv)
	data = pickle.load(open(os.getcwd()+"/pickle_files/"+data_file,'rb'))
	Xs,Y,feats = data[0],data[1],data[2]
	classify_with_scikit(Xs,Y,data_set)
	print_most_informative_features(feats,data_set)
	

def choose_data_type_and_set(argv):
	
	""" Return name of data file, based on type of tweets provided by the user """
	
	data_type, data_set, error = "","",False
	try:
		data_type = argv[1].lower().strip()
		data_set = argv[2].lower().strip()
		if data_set != "dev" and data_set != "total":
			error = True
	except:
		error = True
	
	if data_type == "all":
		data_file = "data_all.p"
	elif data_type == "personal":
		data_file = "data_personal.p"
	elif data_type == "retweets":
		data_file = "data_RT.p"
	else:
		error = True
	if error == True:
		print("\n\nArguments not understood, please call this program by typing one of the following options in the Linux command shell:")
		print("\nPython3 classify_data.py all dev \nPython3 classify_data.py all total  \nPython3 classify_data.py personal dev\nPython3 classify_data.py personal total\nPython3 classify_data.py retweets dev \nPython3 classify_data.py retweets total\n\n")
		exit()
	else:
		print("\n\nType of tweets:",data_type)
		print("Type of data set:",data_set)
	
	
	return data_file, data_set
	
		
def classify_with_scikit(Xs,Y, data_set):	# Based on code provided by dr. L. Bosveld-de Smet and M. Nissim
	
	""" Prints accuracy and precision-recall-f1score table"""
	
	print("\n\nTraining data: 90%\nTest data: 10%\n\n")
	
	for X in Xs:
		split_point = int(0.9*len(X[0]))
		Xtrain = X[0][:split_point]
		Ytrain = Y[:split_point]
		Xtest = X[0][split_point:]
		Ytest = Y[split_point:]
		if data_set == "dev": # reduces data to create development set
			X_dev = Xtrain[:int(0.2*len(Xtrain))]
			Y_dev = Ytrain[:int(0.2*len(Ytrain))]
			split_point = int(0.9*len(X_dev))
			Xtrain = X_dev[:split_point]
			Ytrain = Y_dev[:split_point]
			Xtest = X_dev[split_point:]
			Ytest = Y_dev[split_point:]
		
		vecNB = CountVectorizer()
		vecSVM = TfidfVectorizer()

		classifiers = [Pipeline( [('vec', vecNB),
							('cls', MultinomialNB())] ),
					   Pipeline( [('vec', vecSVM),
							('cls', svm.LinearSVC())] )]
		cur = 0
		for classifier in classifiers:
			print("Feature:",X[1])
			classifier.fit(Xtrain, Ytrain)
			Yguess = classifier.predict(Xtest)
			if cur == 0: 
				print("Classification: Naive Bayes")
				print("Vectorizer: Count")
			else:
				print("Classification: SVM")
				print("Vectorizer: td-idf")
			print("Accuracy:",accuracy_score(Ytest, Yguess),'\n')
			print(classification_report(Ytest,Yguess))	
			print("\n\n\n")
			cur += 1
			  


def print_most_informative_features(feats, data_set):	# Based on code provided by dr. A. Toral Ruiz
	
	""" Prints most informative features, based on Naive Bayes classifying with NLTK """
	
	for feat in feats:
		print("\n\n\nCalculating most informative features of",feat[1],"...")
		train_feats = []
		split = 0.9
		cutoff = int(len(feat[0]) * split)
		
		if data_set == "dev": # reduces data to create development set
			cutoff = int(cutoff * 0.2)
			
		train_feats = feat[0][:cutoff]
		classifier = nltk.classify.NaiveBayesClassifier.train(train_feats)
		classifier.show_most_informative_features(50)


if __name__ == "__main__":
	main(sys.argv)
		
