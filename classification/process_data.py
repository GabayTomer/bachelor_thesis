# !/usr/bin/env python3
# Tomer Gabay (s2726769)
# Bachelor Thesis
# 05-06-17
# Python3
# Linux command: Python3 process_data.py
# Edit lines 60 & 115-117 when changing tweet type

import os
import pickle
from nltk import bigrams, trigrams
from featx import bag_of_words
from collections import Counter
from nltk.tokenize import TweetTokenizer

def main():
	process_data()

def process_data():
	
	""" Creates pickle file with all data in right format to be classified by both Scikit-Learn and NLTK"""
	
	tw_tokenizer = TweetTokenizer()
	committing_tweets_dict = {} # key = username, value = (party committting tweet, party committed to)
	committing_tweets_f = open(os.getcwd()+'/txt_files/4000_party_committing_tweets.txt','r')
	data = pickle.load(open(os.getcwd()+'/pickle_files/data_raw.p','rb'),encoding='latin1') #neccesary, as raw_data.p has been made by Python2
	
	X = []
	Xbi = []
	Xtri = []
	Xfriends = []
	Xhashtags = []
	Xpunctuation = []
	Y = []
	total_unigrams = []
	total_bigrams = []
	total_trigrams = []
	total_friends = []
	total_hashtags = []
	cur = 0
	
	print("Processing data of 4000 users...\n")	
	
	for line in committing_tweets_f:
		line = line.strip().split('	')
		username = line[0]
		party_committing_tweet = tw_tokenizer.tokenize(line[1].strip())
		party = line[2]
		tweets = data[username][0]
		friends = data[username][1]
		tempX = []
		tempBiX = []
		tempBiX2 = []
		tempTriX = []
		tempTriX2 = []
		tempHashtags = []
		for tweet in tweets:
			tweet = tweet.lower().strip()
			tweet = tw_tokenizer.tokenize(tweet)
			if tweet != party_committing_tweet: #	"and 'rt' (not) in tweet:" # set here the filter for which type of tweets. SEE LINE 114-116
				for word in tweet:
					if word[0] == '#':	# split hashtags from grams
						tempHashtags.append(word)
						total_hashtags.append(word)
					else:
						tempX.append(word)
						total_unigrams.append(word)
				tempBiX2.append(list(bigrams(tweet)))
				tempTriX2.append(list(trigrams(tweet)))
		for tweet_bigrams in tempBiX2:
			for bigram in tweet_bigrams:
				if bigram[0][0] != '#' and bigram[1][0] != '#': # split hashtags from grams
					bigram = bigram[0]+"___"+bigram[1] # convert tuple of grams to string
					tempBiX.append(bigram)
					total_bigrams.append(bigram)
		for tweet_trigrams in tempTriX2:
			for trigram in tweet_trigrams:
				if trigram[0][0] != '#' and trigram[1][0] != '#' and trigram[2][0] != '#': # split hashtags from grams
					trigram = trigram[0]+"___"+trigram[1]+"___"+trigram[2] # convert tuple of grams to string
					tempTriX.append(trigram)
					total_trigrams.append(trigram)
		for friend in friends:
			total_friends.append(friend)
		X.append(" ".join(tempX))
		Xbi.append(" ".join(tempBiX))
		Xtri.append(" ".join(tempTriX))
		Xfriends.append(" ".join(friends))
		Xhashtags.append(" ".join(tempHashtags))
		Y.append(party) # party is the label
		cur += 1
		if cur % 500 == 0:
			print("Data of {}/4000 users processed".format(cur))
	
	for f in [(total_unigrams,'unigrams'),(total_bigrams,'bigrams'),(total_trigrams,'trigrams'),(total_friends,'friends'),(total_hashtags,'hashtags')]:
		print_stats(f)
		
	# create counters to delete unique feature vectors (tip from dr. A. Toral Ruiz)
	uni_counter = Counter(total_unigrams)
	bi_counter = Counter(total_bigrams)
	tri_counter = Counter(total_trigrams)
	friends_counter = Counter(total_friends)
	hashtags_counter = Counter(total_hashtags)
	print("\n\n\nDone creating counters")
	
	# create feats for classifying with NLTK instead of Scikit-Learn (based on code provided by dr. A. Toral Ruiz in the course Information Retrieval)
	uni_feats = create_feats(X,Y,uni_counter)
	bi_feats = create_feats(Xbi,Y,bi_counter)
	tri_feats = create_feats(Xtri,Y,tri_counter)
	friends_feats = create_feats(Xfriends,Y,friends_counter)
	hashtags_feats = create_feats(Xhashtags,Y,hashtags_counter)
	print("\n\n\nDone creating features")
	
	total_data = [(X,'unigrams'),(Xbi,'bigrams'),(Xtri,'trigrams'),(Xfriends,'friends'),(Xhashtags,'hashtags')],Y,[(uni_feats,'unigrams'),(bi_feats,'bigrams'),(tri_feats,' trigrams'),(friends_feats,'friends'),(hashtags_feats,'hashtags')]
	
	#pickle.dump(total_data,open(os.getcwd()+'/pickle_files/data_all.p','wb'))
	#pickle.dump(total_data,open(os.getcwd()+'/pickle_files/data_personal.p','wb'))	# EDIT THIS BASED ON DATA TYPE
	#pickle.dump(total_data,open(os.getcwd()+'/pickle_files/data_RT.p','wb'))
	#print("\n\n\nDone dumping data")

def print_stats(f):
	
	""" Prints total vectors, unique vectors, and percentage unique vectors of total vectors """
	
	print("\n\nTotal {}: {}\nUnique {}: {} ({}%)".format(f[1],len(f[0]),f[1],len(set(f[0])),round(len(set(f[0])) / len(f[0])*100,1)))
	
	
def create_feats(X,Y,total_counter):
	
	""" Create list with tuple (*dictionary returned by on bag_of_words()*, *party*) """
	
	feats = []
	for i in range(len(X)):
		tempfeats = []
		for item in X[i].split():
			if total_counter[item] >1:	# on advise of dr. A. Toral Ruiz
				tempfeats.append(item)
		feats.append((bag_of_words(tempfeats),Y[i])) # bag_of_words() from A. Toral Ruiz's featx.py
	return feats
	
if __name__ == "__main__":
	main()
