# !/usr/bin/env python3
# Tomer Gabay (s2726769)
# Bachelor Thesis
# 05-06-17
# Python3
# Linux command: Python3 annotate_data.py

import os
import random

def main():
	generate_annotation_tweets(os.getcwd()+"/txt_files/4000_party_committing_tweets.txt", os.getcwd()+"/txt_files/pre_annotated_tweets.txt")
	annotate_tweets(os.getcwd()+"/txt_files/pre_annotated_tweets.txt", os.getcwd()+"/txt_files/annotated_tweets.txt")
	
def generate_annotation_tweets(pre_anno, to_anno):
	
	""" Writes 100 random lines of input file to output file """
	
	f = open(pre_anno,'r')
	out = open(to_anno,'w')
	com_tweets_list = [(line.split('	')[1],line.split('	')[2]) for line in f]
	random_tweets = random.sample(com_tweets_list,100) # collect randomly 100 tweets to be annotated
	for line in random_tweets:
		outline = line[0]+'	'+line[1]
		out.write(outline)
	f.close()
	out.close()


def annotate_tweets(pre_anno, annotated):
	
	""" Ask user to annotate tweets, write results to new file """
	
	f = open(pre_anno,"r")
	f_out = open(annotated,"w")
	pre_ano_tweets = [line.strip().split('	') for line in f]
	ano_tweets = []
	x = 0
	while x < 101:
		print ("\n\n\n\n\n\n\nDoes this tweet implicates that the (re)tweeter will or has voted for "+ pre_ano_tweets[x][1]+" only, or is there a call to vote for "+pre_ano_tweets[x][1]+" only, in the tweet? [Y/N]["+str(x)+"/100]")
		print("\n\n"+pre_ano_tweets[x][1]+"\n\n")
		answer = str(raw_input("---  '"+pre_ano_tweets[x][0]+"'   ---\n"))
		try:
			if answer.lower()[0] == "y":
				out = "	".join(pre_ano_tweets[x])+"	"+"Y\n"
				f_out.write(out)
				x += 1
			elif answer.lower()[0] == "n":
				out = "	".join(pre_ano_tweets[x])+"	"+"N\n"
				f_out.write(out)
				x += 1
			else:
				print("Please press y for yes, n for no")
		except:
			print("Something went wrong, the same tweet will reappear")
	f.close()
	f_out.close()
		
if __name__ == "__main__":
	main()
