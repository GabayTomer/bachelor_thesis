Processing data of 4000 users...

Data of 500/4000 users processed
Data of 1000/4000 users processed
Data of 1500/4000 users processed
Data of 2000/4000 users processed
Data of 2500/4000 users processed
Data of 3000/4000 users processed
Data of 3500/4000 users processed
Data of 4000/4000 users processed


Total unigrams: 13221961
Unique unigrams: 694244 (5.3%)


Total bigrams: 12208049
Unique bigrams: 3019099 (24.7%)


Total trigrams: 11292568
Unique trigrams: 5812553 (51.5%)


Total friends: 664770
Unique friends: 259024 (39.0%)


Total hashtags: 455943
Unique hashtags: 79952 (17.5%)



Done creating counters



Done creating features