Processing data of 4000 users...

Data of 500/4000 users processed
Data of 1000/4000 users processed
Data of 1500/4000 users processed
Data of 2000/4000 users processed
Data of 2500/4000 users processed
Data of 3000/4000 users processed
Data of 3500/4000 users processed
Data of 4000/4000 users processed


Total unigrams: 8382136
Unique unigrams: 424921 (5.1%)


Total bigrams: 7780736
Unique bigrams: 1806372 (23.2%)


Total trigrams: 7235447
Unique trigrams: 3318301 (45.9%)


Total friends: 664770
Unique friends: 259024 (39.0%)


Total hashtags: 288854
Unique hashtags: 49251 (17.1%)



Done creating counters



Done creating features



Done dumping data