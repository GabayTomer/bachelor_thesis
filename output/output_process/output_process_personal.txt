Processing data of 4000 users...

Data of 500/4000 users processed
Data of 1000/4000 users processed
Data of 1500/4000 users processed
Data of 2000/4000 users processed
Data of 2500/4000 users processed
Data of 3000/4000 users processed
Data of 3500/4000 users processed
Data of 4000/4000 users processed


Total unigrams: 4839825
Unique unigrams: 389425 (8.0%)


Total bigrams: 4427313
Unique bigrams: 1647533 (37.2%)


Total trigrams: 4057121
Unique trigrams: 2941946 (72.5%)


Total friends: 664770
Unique friends: 259024 (39.0%)


Total hashtags: 167089
Unique hashtags: 44987 (26.9%)



Done creating counters



Done creating features



Done dumping data