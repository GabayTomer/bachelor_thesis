Type of tweets: personal
Type of data set: total


Training data: 90%
Test data: 10%


Feature: unigrams
Classification: Naive Bayes
Vectorizer: Count
Accuracy: 0.705 

             precision    recall  f1-score   support

        CDA       0.67      0.77      0.72        97
        D66       0.73      0.43      0.54       102
 GroenLinks       0.58      0.79      0.67        90
        PVV       0.87      0.83      0.85       111

avg / total       0.72      0.70      0.70       400





Feature: unigrams
Classification: SVM
Vectorizer: td-idf
Accuracy: 0.81 

             precision    recall  f1-score   support

        CDA       0.88      0.81      0.84        97
        D66       0.80      0.62      0.70       102
 GroenLinks       0.73      0.91      0.81        90
        PVV       0.85      0.90      0.87       111

avg / total       0.81      0.81      0.81       400





Feature: bigrams
Classification: Naive Bayes
Vectorizer: Count
Accuracy: 0.735 

             precision    recall  f1-score   support

        CDA       0.80      0.77      0.79        97
        D66       0.80      0.46      0.58       102
 GroenLinks       0.57      0.92      0.70        90
        PVV       0.88      0.80      0.84       111

avg / total       0.77      0.73      0.73       400





Feature: bigrams
Classification: SVM
Vectorizer: td-idf
Accuracy: 0.7875 

             precision    recall  f1-score   support

        CDA       0.86      0.79      0.82        97
        D66       0.84      0.55      0.66       102
 GroenLinks       0.69      0.91      0.78        90
        PVV       0.81      0.90      0.85       111

avg / total       0.80      0.79      0.78       400





Feature: trigrams
Classification: Naive Bayes
Vectorizer: Count
Accuracy: 0.72 

             precision    recall  f1-score   support

        CDA       0.85      0.75      0.80        97
        D66       0.81      0.45      0.58       102
 GroenLinks       0.53      0.96      0.68        90
        PVV       0.88      0.75      0.81       111

avg / total       0.78      0.72      0.72       400





Feature: trigrams
Classification: SVM
Vectorizer: td-idf
Accuracy: 0.755 

             precision    recall  f1-score   support

        CDA       0.82      0.74      0.78        97
        D66       0.79      0.55      0.65       102
 GroenLinks       0.68      0.87      0.76        90
        PVV       0.76      0.86      0.81       111

avg / total       0.76      0.76      0.75       400





Feature: friends
Classification: Naive Bayes
Vectorizer: Count
Accuracy: 0.865 

             precision    recall  f1-score   support

        CDA       0.96      0.88      0.91        97
        D66       0.82      0.75      0.78       102
 GroenLinks       0.81      0.92      0.86        90
        PVV       0.89      0.92      0.90       111

avg / total       0.87      0.86      0.86       400





Feature: friends
Classification: SVM
Vectorizer: td-idf
Accuracy: 0.8825 

             precision    recall  f1-score   support

        CDA       0.96      0.91      0.93        97
        D66       0.87      0.77      0.82       102
 GroenLinks       0.80      0.94      0.87        90
        PVV       0.91      0.91      0.91       111

avg / total       0.89      0.88      0.88       400





Feature: hashtags
Classification: Naive Bayes
Vectorizer: Count
Accuracy: 0.61 

             precision    recall  f1-score   support

        CDA       0.84      0.72      0.78        97
        D66       0.64      0.53      0.58       102
 GroenLinks       0.41      0.68      0.51        90
        PVV       0.69      0.53      0.60       111

avg / total       0.65      0.61      0.62       400





Feature: hashtags
Classification: SVM
Vectorizer: td-idf
Accuracy: 0.7025 

             precision    recall  f1-score   support

        CDA       0.84      0.67      0.75        97
        D66       0.65      0.55      0.60       102
 GroenLinks       0.71      0.67      0.69        90
        PVV       0.65      0.90      0.76       111

avg / total       0.71      0.70      0.70       400








Calculating most informative features of unigrams ...
Most Informative Features
                empathie = True           GroenL : PVV    =     79.1 : 1.0
          @frenkie4allll = True              PVV : CDA    =     74.8 : 1.0
            @peteranshof = True              PVV : GroenL =     72.7 : 1.0
                duurzame = True              D66 : PVV    =     69.0 : 1.0
             @jacogeurts = True              CDA : PVV    =     62.7 : 1.0
            duurzaamheid = True              D66 : PVV    =     61.0 : 1.0
            inspirerende = True              CDA : PVV    =     55.5 : 1.0
              werkbezoek = True              CDA : PVV    =     51.0 : 1.0
            @ruthpeetoom = True              CDA : PVV    =     50.9 : 1.0
                december = True              CDA : PVV    =     50.9 : 1.0
            @hannesz1956 = True              PVV : GroenL =     50.2 : 1.0
         @realdutchroots = True              PVV : CDA    =     49.8 : 1.0
         @lisahenegauwen = True              PVV : GroenL =     47.4 : 1.0
                    turk = True              PVV : GroenL =     40.8 : 1.0
        raadsvergadering = True              CDA : PVV    =     40.8 : 1.0
          @tanjameijer42 = True              PVV : CDA    =     40.3 : 1.0
             inspirerend = True              D66 : PVV    =     39.9 : 1.0
            communicatie = True              CDA : PVV    =     39.1 : 1.0
          @vera_bergkamp = True              D66 : PVV    =     38.6 : 1.0
           @rvravenstijn = True              PVV : CDA    =     36.2 : 1.0
            @samvanrooy1 = True              PVV : GroenL =     35.8 : 1.0
             progressief = True           GroenL : PVV    =     35.8 : 1.0
            @monakeijzer = True              CDA : GroenL =     35.5 : 1.0
              toegewenst = True              CDA : PVV    =     35.1 : 1.0
           @truusdemierr = True              PVV : CDA    =     34.9 : 1.0
            @v_of_europe = True              PVV : CDA    =     34.9 : 1.0
                  meetup = True           GroenL : CDA    =     34.7 : 1.0
                     msm = True              PVV : GroenL =     34.5 : 1.0
           @jangajentaan = True              PVV : GroenL =     33.8 : 1.0
               regionale = True              CDA : PVV    =     33.7 : 1.0
             @ziltebotte = True              PVV : GroenL =     33.5 : 1.0
            islamisering = True              PVV : CDA    =     32.8 : 1.0
              uitvoering = True              CDA : PVV    =     32.5 : 1.0
         @mvantoorenburg = True              CDA : GroenL =     31.2 : 1.0
              ondersteun = True              CDA : GroenL =     31.2 : 1.0
                     wmo = True              CDA : PVV    =     31.2 : 1.0
               @widtvoet = True              PVV : GroenL =     31.0 : 1.0
                kaartjes = True           GroenL : PVV    =     30.9 : 1.0
               @henkbres = True              PVV : CDA    =     30.8 : 1.0
            buitengebied = True              CDA : PVV    =     30.5 : 1.0
             @svenstweet = True              PVV : GroenL =     30.4 : 1.0
             @zihniozdil = True           GroenL : CDA    =     30.1 : 1.0
                      ov = True           GroenL : PVV    =     29.6 : 1.0
               wethouder = True              CDA : PVV    =     29.6 : 1.0
               leugenaar = True              PVV : CDA    =     29.5 : 1.0
              @wkoolmees = True              D66 : GroenL =     29.4 : 1.0
          @pieteromtzigt = True              CDA : GroenL =     29.4 : 1.0
          laagdrempelige = True              CDA : GroenL =     29.2 : 1.0
             leenstelsel = True              CDA : PVV    =     29.2 : 1.0
             rondleiding = True              CDA : PVV    =     29.2 : 1.0



Calculating most informative features of bigrams ...
Most Informative Features
         eerlijk___delen = True           GroenL : D66    =     70.1 : 1.0
       stem___groenlinks = True           GroenL : D66    =     67.4 : 1.0
              delen___en = True           GroenL : D66    =     65.5 : 1.0
            empathie___, = True           GroenL : CDA    =     65.2 : 1.0
       schone___toekomst = True           GroenL : CDA    =     63.2 : 1.0
               is___tijd = True           GroenL : D66    =     57.4 : 1.0
                 :___cda = True              CDA : PVV    =     50.2 : 1.0
              stem___pvv = True              PVV : GroenL =     50.2 : 1.0
               van___cda = True              CDA : GroenL =     49.4 : 1.0
                 cda___: = True              CDA : GroenL =     49.0 : 1.0
       een___progressief = True           GroenL : CDA    =     48.6 : 1.0
   progressief___kabinet = True           GroenL : CDA    =     48.0 : 1.0
       mooi___initiatief = True              CDA : PVV    =     44.3 : 1.0
             volle___bak = True              CDA : PVV    =     44.3 : 1.0
          verbinding___. = True           GroenL : PVV    =     44.0 : 1.0
       voor___verbinding = True           GroenL : D66    =     41.1 : 1.0
          campagne___van = True              CDA : PVV    =     41.0 : 1.0
                cda___in = True              CDA : PVV    =     40.4 : 1.0
             ,___eerlijk = True           GroenL : PVV    =     40.1 : 1.0
            voor___nieuw = True           GroenL : PVV    =     38.9 : 1.0
     nieuw___leiderschap = True           GroenL : CDA    =     38.3 : 1.0
         leiderschap___. = True           GroenL : PVV    =     38.1 : 1.0
                   ___ = True           GroenL : PVV    =     37.4 : 1.0
        initiatief___van = True              CDA : PVV    =     37.1 : 1.0
            die___turken = True              PVV : CDA    =     36.9 : 1.0
         @groenlinks___. = True           GroenL : CDA    =     36.1 : 1.0
                uur___in = True              CDA : PVV    =     35.8 : 1.0
          aangenomen___. = True              CDA : PVV    =     35.8 : 1.0
           het___klimaat = True           GroenL : PVV    =     34.8 : 1.0
       @sybrandbuma___in = True              CDA : D66    =     34.8 : 1.0
          sybrand___buma = True              CDA : GroenL =     34.1 : 1.0
         op___werkbezoek = True              CDA : PVV    =     33.8 : 1.0
           vandaag___met = True              CDA : PVV    =     33.8 : 1.0
            werken___aan = True              CDA : PVV    =     33.1 : 1.0
                  ?___;) = True           GroenL : PVV    =     32.2 : 1.0
             van___start = True              CDA : PVV    =     32.1 : 1.0
               cda___wil = True              CDA : PVV    =     32.1 : 1.0
                appã___¨ = True              CDA : GroenL =     31.9 : 1.0
             start___van = True              CDA : PVV    =     31.8 : 1.0
            namens___het = True              CDA : GroenL =     31.2 : 1.0
              cda___voor = True              CDA : PVV    =     31.2 : 1.0
          afscheid___van = True              CDA : PVV    =     30.9 : 1.0
                 :___d66 = True              D66 : GroenL =     30.7 : 1.0
        mooi___resultaat = True              CDA : PVV    =     30.5 : 1.0
             met___jesse = True           GroenL : D66    =     30.2 : 1.0
        pieter___omtzigt = True              CDA : D66    =     30.2 : 1.0
                cda___de = True              CDA : D66    =     30.2 : 1.0
                .___@d66 = True              D66 : GroenL =     30.1 : 1.0
          groenlinks___. = True           GroenL : CDA    =     29.9 : 1.0
         .___@cdavandaag = True              CDA : PVV    =     29.9 : 1.0



Calculating most informative features of  trigrams ...
Most Informative Features
         het___is___tijd = True           GroenL : D66    =     95.7 : 1.0
        is___tijd___voor = True           GroenL : D66    =     90.5 : 1.0
 een___schone___toekomst = True           GroenL : CDA    =     63.2 : 1.0
voor___nieuw___leiderschap = True           GroenL : D66    =     62.8 : 1.0
 nieuw___leiderschap___. = True           GroenL : CDA    =     61.9 : 1.0
   stem___groenlinks___. = True           GroenL : D66    =     61.5 : 1.0
       toekomst___.___ik = True           GroenL : CDA    =     61.2 : 1.0
     tijd___voor___nieuw = True           GroenL : D66    =     49.7 : 1.0
        voor___het___cda = True              CDA : GroenL =     46.7 : 1.0
een___progressief___kabinet = True           GroenL : CDA    =     45.3 : 1.0
      jesse___klaver___: = True           GroenL : CDA    =     43.3 : 1.0
    met___jesse___klaver = True           GroenL : PVV    =     43.3 : 1.0
voor___een___progressief = True           GroenL : D66    =     42.4 : 1.0
        het___eens___met = True           GroenL : CDA    =     41.3 : 1.0
         van___het___cda = True              CDA : PVV    =     40.6 : 1.0
               ______ = True           GroenL : PVV    =     37.4 : 1.0
         land___dat___we = True              CDA : GroenL =     33.9 : 1.0
   door___willen___geven = True              CDA : PVV    =     33.8 : 1.0
         dat___we___door = True              CDA : D66    =     33.5 : 1.0
      we___door___willen = True              CDA : PVV    =     33.1 : 1.0
           het___cda___: = True              CDA : D66    =     32.2 : 1.0
            appã___¨___l = True              CDA : GroenL =     31.9 : 1.0
         in___de___regio = True              CDA : PVV    =     30.5 : 1.0
        een___land___dat = True              CDA : D66    =     29.5 : 1.0
            !___doe___je = True           GroenL : PVV    =     29.0 : 1.0
   installeer___de___app = True              CDA : GroenL =     28.6 : 1.0
          in___de___raad = True              CDA : PVV    =     28.6 : 1.0
              ¨___l___en = True              CDA : D66    =     28.2 : 1.0
       voor___een___land = True              CDA : D66    =     27.5 : 1.0
        hoe___goed___ken = True              CDA : GroenL =     27.2 : 1.0
        goed___ken___jij = True              CDA : GroenL =     27.2 : 1.0
          je___ook___mee = True           GroenL : PVV    =     27.0 : 1.0
          dag___van___de = True              CDA : PVV    =     26.6 : 1.0
     aanwezig___bij___de = True              CDA : PVV    =     25.9 : 1.0
        op___de___agenda = True              CDA : PVV    =     25.9 : 1.0
       op___bezoek___bij = True              CDA : PVV    =     25.5 : 1.0
             met___o___. = True              CDA : PVV    =     25.4 : 1.0
         in___de___trein = True           GroenL : PVV    =     25.2 : 1.0
         ik___heb___deze = True           GroenL : PVV    =     25.1 : 1.0
           .___het___cda = True              CDA : D66    =     24.9 : 1.0
          /___nieuws___/ = True              CDA : GroenL =     24.5 : 1.0
               ______ð = True           GroenL : CDA    =     23.5 : 1.0
      voor___een___beter = True              CDA : PVV    =     23.3 : 1.0
         de___markt___in = True              CDA : PVV    =     23.3 : 1.0
           jij___ook___? = True           GroenL : PVV    =     22.9 : 1.0
      veel___succes___en = True              CDA : PVV    =     22.6 : 1.0
  looking___forward___to = True           GroenL : PVV    =     22.5 : 1.0
            zin___in___! = True           GroenL : PVV    =     22.5 : 1.0
harte___gefeliciteerd___met = True              CDA : PVV    =     22.2 : 1.0
   ben___benieuwd___naar = True           GroenL : PVV    =     21.8 : 1.0



Calculating most informative features of friends ...
Most Informative Features
          JoostNiemoller = True              PVV : GroenL =    132.7 : 1.0
             V_of_Europe = True              PVV : CDA    =    108.0 : 1.0
         Esther_de_Lange = True              CDA : GroenL =    107.2 : 1.0
            JAWCJanssens = True              CDA : PVV    =    106.0 : 1.0
         StuivenbergInfo = True              PVV : GroenL =    101.3 : 1.0
          AgnesMulderCDA = True              CDA : PVV    =     98.1 : 1.0
          RealDutchRoots = True              PVV : GroenL =     91.1 : 1.0
            RobertKnijff = True              PVV : GroenL =     91.1 : 1.0
            PeterSweden7 = True              PVV : CDA    =     87.7 : 1.0
           frenkie4allll = True              PVV : GroenL =     87.0 : 1.0
            LettyDemmers = True              D66 : GroenL =     82.1 : 1.0
               waziman15 = True              PVV : CDA    =     81.6 : 1.0
              Martijncda = True              CDA : PVV    =     80.4 : 1.0
             RijnPatrick = True              PVV : GroenL =     80.2 : 1.0
             ruthpeetoom = True              CDA : GroenL =     78.8 : 1.0
                 d_e_mol = True              PVV : GroenL =     77.5 : 1.0
             rikgrashoff = True           GroenL : PVV    =     75.9 : 1.0
           VictorKortijs = True              PVV : CDA    =     75.5 : 1.0
         tRadicaleMidden = True              D66 : CDA    =     75.4 : 1.0
             Hannesz1956 = True              PVV : CDA    =     74.8 : 1.0
          Veerman_Edward = True              PVV : GroenL =     74.7 : 1.0
          michieldijkman = True              CDA : PVV    =     74.5 : 1.0
                 Fub_Fub = True              PVV : GroenL =     74.0 : 1.0
              JanTerlouw = True              D66 : PVV    =     73.6 : 1.0
             2dekamerpvv = True              PVV : GroenL =     72.0 : 1.0
               cdaeuropa = True              CDA : GroenL =     70.2 : 1.0
               Mirjam152 = True              PVV : CDA    =     68.1 : 1.0
            SalimaBelhaj = True              D66 : PVV    =     67.6 : 1.0
            truusdemierr = True              PVV : GroenL =     67.2 : 1.0
                hulswood = True              PVV : CDA    =     66.4 : 1.0
              ErikRonnes = True              CDA : GroenL =     66.2 : 1.0
                  ovcned = True              PVV : D66    =     65.0 : 1.0
          Blondemevrouw1 = True              PVV : CDA    =     64.8 : 1.0
         charlotte101987 = True              PVV : CDA    =     64.7 : 1.0
             neeRUTTEnee = True              PVV : CDA    =     64.7 : 1.0
            MLP_officiel = True              PVV : CDA    =     64.0 : 1.0
         horentastenzien = True              PVV : GroenL =     63.8 : 1.0
             jpaternotte = True              D66 : PVV    =     63.7 : 1.0
          SybrenKooistra = True           GroenL : CDA    =     63.2 : 1.0
               DFranksen = True              PVV : GroenL =     63.1 : 1.0
            PVVstrijders = True              PVV : CDA    =     62.6 : 1.0
             SamvanRooy1 = True              PVV : CDA    =     61.3 : 1.0
          Jan_Pijnenburg = True              PVV : GroenL =     61.2 : 1.0
          Little_one_Ann = True              PVV : GroenL =     60.4 : 1.0
              HitradioNL = True              PVV : GroenL =     60.4 : 1.0
             VMStichting = True              D66 : GroenL =     59.8 : 1.0
          Real_Pantheist = True              PVV : GroenL =     59.7 : 1.0
         consumentnieuws = True              PVV : GroenL =     59.7 : 1.0
           destembus2017 = True           GroenL : PVV    =     58.9 : 1.0
         anitahendriks68 = True              PVV : CDA    =     58.6 : 1.0



Calculating most informative features of hashtags ...
Most Informative Features
    #stemvoorverandering = True           GroenL : D66    =    196.4 : 1.0
                #stemd66 = True              D66 : GroenL =    131.4 : 1.0
             #cdacongres = True              CDA : D66    =     79.9 : 1.0
                #teamcda = True              CDA : PVV    =     78.4 : 1.0
    #tijdvoorverandering = True           GroenL : CDA    =     72.4 : 1.0
                #stempvv = True              PVV : CDA    =     59.5 : 1.0
             #d66congres = True              D66 : GroenL =     57.1 : 1.0
                  #nexit = True              PVV : CDA    =     55.9 : 1.0
           #ingesprekmet = True              D66 : CDA    =     52.0 : 1.0
                #stemcda = True              CDA : PVV    =     43.0 : 1.0
         #beternederland = True              CDA : D66    =     38.1 : 1.0
              #onderwijs = True              D66 : PVV    =     36.0 : 1.0
                 #turken = True              PVV : CDA    =     32.2 : 1.0
                 #nldoet = True              CDA : PVV    =     31.8 : 1.0
#nietschreeuwenmaarpraten = True           GroenL : CDA    =     31.3 : 1.0
                #klimaat = True           GroenL : PVV    =     30.9 : 1.0
                  #islam = True              PVV : CDA    =     29.5 : 1.0
               #demeetup = True           GroenL : CDA    =     29.4 : 1.0
    #nederlandweervanons = True              PVV : GroenL =     26.3 : 1.0
                 #meetup = True           GroenL : CDA    =     24.8 : 1.0
                  #bumor = True              CDA : GroenL =     20.5 : 1.0
              #slotdebat = True           GroenL : D66    =     19.2 : 1.0
          #vrijwilligers = True              CDA : GroenL =     18.5 : 1.0
                #wietwet = True              D66 : GroenL =     17.9 : 1.0
                    #vnl = True              PVV : GroenL =     17.4 : 1.0
                   #widm = True           GroenL : PVV    =     16.9 : 1.0
                 #turkse = True              PVV : GroenL =     16.7 : 1.0
              #glcongres = True           GroenL : PVV    =     16.6 : 1.0
                #twibbon = True              D66 : PVV    =     16.2 : 1.0
                #moslims = True              PVV : GroenL =     16.0 : 1.0
                   #pvvâ = True              PVV : CDA    =     15.9 : 1.0
                  #zinin = True              CDA : PVV    =     15.6 : 1.0
            #kiesklimaat = True           GroenL : CDA    =     15.5 : 1.0
                   #cdaâ = True              CDA : PVV    =     15.4 : 1.0
                #moltalk = True           GroenL : PVV    =     15.3 : 1.0
                     #fd = True              D66 : PVV    =     15.2 : 1.0
                #pvv2017 = True              PVV : GroenL =     14.7 : 1.0
              #markrutte = True              PVV : CDA    =     14.6 : 1.0
               #geenpeil = True              PVV : CDA    =     14.6 : 1.0
           #worldtotrump = True           GroenL : CDA    =     13.6 : 1.0
               #nijmegen = True           GroenL : PVV    =     13.4 : 1.0
                   #ceta = True           GroenL : PVV    =     12.7 : 1.0
                    #d66 = True              D66 : GroenL =     12.4 : 1.0
     #klimaatverandering = True           GroenL : CDA    =     12.2 : 1.0
            #jessemeetup = True           GroenL : D66    =     12.2 : 1.0
                 #zwolle = True              CDA : PVV    =     12.1 : 1.0
           #grenzendicht = True              PVV : D66    =     11.9 : 1.0
               #ikdoemee = True              CDA : GroenL =     11.8 : 1.0
                   #paay = True              PVV : GroenL =     11.3 : 1.0
                  #vrouw = True              PVV : D66    =     11.1 : 1.0