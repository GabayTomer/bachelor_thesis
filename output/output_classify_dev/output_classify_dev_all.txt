Type of tweets: all
Type of data set: dev


Training data: 90%
Test data: 10%


Feature: unigrams
Classification: Naive Bayes
Vectorizer: Count
Accuracy: 0.75 

             precision    recall  f1-score   support

        CDA       0.75      0.90      0.82        20
        D66       0.67      0.40      0.50        15
 GroenLinks       0.79      0.65      0.71        17
        PVV       0.76      0.95      0.84        20

avg / total       0.74      0.75      0.73        72





Feature: unigrams
Classification: SVM
Vectorizer: td-idf
Accuracy: 0.847222222222 

             precision    recall  f1-score   support

        CDA       0.95      0.95      0.95        20
        D66       0.78      0.47      0.58        15
 GroenLinks       0.80      0.94      0.86        17
        PVV       0.83      0.95      0.88        20

avg / total       0.84      0.85      0.84        72





Feature: bigrams
Classification: Naive Bayes
Vectorizer: Count
Accuracy: 0.777777777778 

             precision    recall  f1-score   support

        CDA       0.83      0.95      0.88        20
        D66       0.80      0.53      0.64        15
 GroenLinks       0.90      0.53      0.67        17
        PVV       0.69      1.00      0.82        20

avg / total       0.80      0.78      0.76        72





Feature: bigrams
Classification: SVM
Vectorizer: td-idf
Accuracy: 0.833333333333 

             precision    recall  f1-score   support

        CDA       0.95      0.90      0.92        20
        D66       0.80      0.53      0.64        15
 GroenLinks       0.80      0.94      0.86        17
        PVV       0.78      0.90      0.84        20

avg / total       0.84      0.83      0.83        72





Feature: trigrams
Classification: Naive Bayes
Vectorizer: Count
Accuracy: 0.763888888889 

             precision    recall  f1-score   support

        CDA       0.83      0.95      0.88        20
        D66       0.89      0.53      0.67        15
 GroenLinks       1.00      0.47      0.64        17
        PVV       0.62      1.00      0.77        20

avg / total       0.82      0.76      0.75        72





Feature: trigrams
Classification: SVM
Vectorizer: td-idf
Accuracy: 0.777777777778 

             precision    recall  f1-score   support

        CDA       1.00      0.95      0.97        20
        D66       0.67      0.53      0.59        15
 GroenLinks       0.79      0.65      0.71        17
        PVV       0.67      0.90      0.77        20

avg / total       0.79      0.78      0.77        72





Feature: friends
Classification: Naive Bayes
Vectorizer: Count
Accuracy: 0.833333333333 

             precision    recall  f1-score   support

        CDA       0.95      0.90      0.92        20
        D66       0.67      0.53      0.59        15
 GroenLinks       0.79      0.88      0.83        17
        PVV       0.86      0.95      0.90        20

avg / total       0.83      0.83      0.83        72





Feature: friends
Classification: SVM
Vectorizer: td-idf
Accuracy: 0.875 

             precision    recall  f1-score   support

        CDA       0.95      0.95      0.95        20
        D66       0.82      0.60      0.69        15
 GroenLinks       0.84      0.94      0.89        17
        PVV       0.86      0.95      0.90        20

avg / total       0.87      0.88      0.87        72





Feature: hashtags
Classification: Naive Bayes
Vectorizer: Count
Accuracy: 0.861111111111 

             precision    recall  f1-score   support

        CDA       0.86      0.95      0.90        20
        D66       0.90      0.60      0.72        15
 GroenLinks       0.93      0.82      0.87        17
        PVV       0.80      1.00      0.89        20

avg / total       0.87      0.86      0.85        72





Feature: hashtags
Classification: SVM
Vectorizer: td-idf
Accuracy: 0.861111111111 

             precision    recall  f1-score   support

        CDA       0.94      0.80      0.86        20
        D66       1.00      0.60      0.75        15
 GroenLinks       0.81      1.00      0.89        17
        PVV       0.80      1.00      0.89        20

avg / total       0.88      0.86      0.85        72








Calculating most informative features of unigrams ...
Most Informative Features
         @blondemevrouw1 = True              PVV : CDA    =     74.1 : 1.0
            @monakeijzer = True              CDA : GroenL =     70.8 : 1.0
          @frenkie4allll = True              PVV : CDA    =     65.5 : 1.0
            @v_of_europe = True              PVV : GroenL =     63.7 : 1.0
         @lisahenegauwen = True              PVV : CDA    =     62.6 : 1.0
           @jawcjanssens = True              CDA : GroenL =     57.5 : 1.0
            @peteranshof = True              PVV : CDA    =     55.5 : 1.0
         @percolator_hnj = True              PVV : GroenL =     53.5 : 1.0
                @bpschut = True              PVV : GroenL =     52.2 : 1.0
                    turk = True              PVV : GroenL =     52.2 : 1.0
            islamisering = True              PVV : CDA    =     49.0 : 1.0
            @samvanrooy1 = True              PVV : CDA    =     49.0 : 1.0
                 muslims = True              PVV : CDA    =     48.3 : 1.0
           @prisonplanet = True              PVV : CDA    =     47.6 : 1.0
          @pieteromtzigt = True              CDA : GroenL =     45.9 : 1.0
        @martinbosma_pvv = True              PVV : GroenL =     45.6 : 1.0
             @maikel_pvv = True              PVV : CDA    =     45.4 : 1.0
         @joostniemoller = True              PVV : GroenL =     43.6 : 1.0
        @drgertjanmulder = True              PVV : CDA    =     43.3 : 1.0
              werkbezoek = True              CDA : PVV    =     43.2 : 1.0
            @ruthpeetoom = True              CDA : GroenL =     41.0 : 1.0
                      ¼ï = True              PVV : GroenL =     40.6 : 1.0
              loyaliteit = True              PVV : GroenL =     40.6 : 1.0
                    tuig = True              PVV : CDA    =     40.4 : 1.0
                   allah = True              PVV : CDA    =     39.7 : 1.0
            @piadijkstra = True              D66 : PVV    =     37.9 : 1.0
                @fub_fub = True              PVV : CDA    =     37.6 : 1.0
          @svanweyenberg = True              D66 : PVV    =     36.5 : 1.0
             presentatie = True              CDA : PVV    =     35.7 : 1.0
                wegwezen = True              PVV : CDA    =     35.4 : 1.0
             @janterlouw = True              D66 : PVV    =     34.5 : 1.0
             @ziltebotte = True              PVV : CDA    =     33.7 : 1.0
              @michelrog = True              CDA : GroenL =     32.7 : 1.0
           nationaliteit = True              PVV : GroenL =     32.5 : 1.0
               @sypwynia = True              PVV : GroenL =     32.4 : 1.0
              @mirjam152 = True              PVV : CDA    =     31.6 : 1.0
             immigration = True              PVV : CDA    =     31.1 : 1.0
                @keesvee = True              D66 : GroenL =     30.6 : 1.0
          @tanjameijer42 = True              PVV : GroenL =     30.3 : 1.0
                 colonne = True              PVV : CDA    =     30.3 : 1.0
               gezellige = True              CDA : PVV    =     29.5 : 1.0
          @vera_bergkamp = True              D66 : GroenL =     29.0 : 1.0
                  sharia = True              PVV : CDA    =     29.0 : 1.0
              landelijke = True              CDA : PVV    =     28.9 : 1.0
        @jongedemocraten = True              D66 : GroenL =     28.5 : 1.0
             fascistisch = True              PVV : GroenL =     28.3 : 1.0
                @ten_gop = True              PVV : CDA    =     28.3 : 1.0
        @snouckhurgronje = True              PVV : D66    =     28.2 : 1.0
           @truusdemierr = True              PVV : D66    =     28.0 : 1.0
               regionale = True              CDA : PVV    =     27.6 : 1.0



Calculating most informative features of bigrams ...
Most Informative Features
    @martinbosma_pvv___: = True              PVV : CDA    =     67.6 : 1.0
   rt___@martinbosma_pvv = True              PVV : CDA    =     67.6 : 1.0
    rt___@blondemevrouw1 = True              PVV : CDA    =     64.8 : 1.0
     @blondemevrouw1___: = True              PVV : CDA    =     64.8 : 1.0
      @pieteromtzigt___: = True              CDA : GroenL =     62.6 : 1.0
        rt___@groenlinks = True           GroenL : CDA    =     61.9 : 1.0
         @groenlinks___: = True           GroenL : CDA    =     61.9 : 1.0
     rt___@frenkie4allll = True              PVV : CDA    =     61.9 : 1.0
      @frenkie4allll___: = True              PVV : CDA    =     61.9 : 1.0
     rt___@pieteromtzigt = True              CDA : GroenL =     60.7 : 1.0
     @lisahenegauwen___: = True              PVV : CDA    =     58.3 : 1.0
    rt___@lisahenegauwen = True              PVV : CDA    =     58.3 : 1.0
      rt___@jawcjanssens = True              CDA : PVV    =     55.6 : 1.0
       @jawcjanssens___: = True              CDA : PVV    =     55.6 : 1.0
   rt___@geertwilderspvv = True              PVV : GroenL =     55.4 : 1.0
          @apechtold___: = True              D66 : PVV    =     53.3 : 1.0
        rt___@cdavandaag = True              CDA : D66    =     53.2 : 1.0
     @percolator_hnj___: = True              PVV : GroenL =     51.5 : 1.0
    rt___@percolator_hnj = True              PVV : GroenL =     51.5 : 1.0
 dubbele___nationaliteit = True              PVV : GroenL =     50.8 : 1.0
         rt___@apechtold = True              D66 : PVV    =     50.6 : 1.0
               rt___@d66 = True              D66 : CDA    =     50.3 : 1.0
         rt___@mirjam152 = True              PVV : CDA    =     49.7 : 1.0
          @mirjam152___: = True              PVV : CDA    =     49.7 : 1.0
                   ___± = True              PVV : GroenL =     49.4 : 1.0
            @bpschut___: = True              PVV : GroenL =     47.4 : 1.0
           rt___@bpschut = True              PVV : GroenL =     47.4 : 1.0
       rt___@sybrandbuma = True              CDA : GroenL =     47.3 : 1.0
      @fleuragemapvv___: = True              PVV : GroenL =     46.7 : 1.0
           @umarebru___: = True              PVV : GroenL =     46.0 : 1.0
      rt___@prisonplanet = True              PVV : CDA    =     45.4 : 1.0
       @prisonplanet___: = True              PVV : CDA    =     45.4 : 1.0
        @samvanrooy1___: = True              PVV : CDA    =     45.4 : 1.0
       rt___@samvanrooy1 = True              PVV : CDA    =     45.4 : 1.0
                  ___³ð = True              PVV : GroenL =     45.3 : 1.0
            @keesvee___: = True              D66 : GroenL =     44.9 : 1.0
        @monakeijzer___: = True              CDA : D66    =     44.8 : 1.0
     rt___@fleuragemapvv = True              PVV : GroenL =     44.7 : 1.0
          rt___@umarebru = True              PVV : GroenL =     44.7 : 1.0
       rt___@monakeijzer = True              CDA : D66    =     44.2 : 1.0
       rt___@peteranshof = True              PVV : CDA    =     44.0 : 1.0
        @peteranshof___: = True              PVV : CDA    =     44.0 : 1.0
           rt___@keesvee = True              D66 : GroenL =     43.5 : 1.0
               cda___wil = True              CDA : GroenL =     42.9 : 1.0
                 .___cda = True              CDA : GroenL =     41.0 : 1.0
                 pvv___! = True              PVV : D66    =     40.8 : 1.0
                   ¬___ = True              D66 : CDA    =     40.7 : 1.0
                  ___¼ï = True              PVV : GroenL =     40.6 : 1.0
                  ¼ï___¸ = True              PVV : GroenL =     40.6 : 1.0
           door___willen = True              CDA : PVV    =     39.4 : 1.0



Calculating most informative features of  trigrams ...
Most Informative Features
rt___@martinbosma_pvv___: = True              PVV : CDA    =     67.6 : 1.0
rt___@blondemevrouw1___: = True              PVV : CDA    =     64.8 : 1.0
    rt___@groenlinks___: = True           GroenL : CDA    =     61.9 : 1.0
 rt___@frenkie4allll___: = True              PVV : CDA    =     61.9 : 1.0
 rt___@pieteromtzigt___: = True              CDA : GroenL =     60.7 : 1.0
rt___@lisahenegauwen___: = True              PVV : CDA    =     58.3 : 1.0
  rt___@jawcjanssens___: = True              CDA : PVV    =     55.6 : 1.0
rt___@geertwilderspvv___: = True              PVV : GroenL =     55.4 : 1.0
    rt___@cdavandaag___: = True              CDA : D66    =     53.2 : 1.0
rt___@percolator_hnj___: = True              PVV : GroenL =     51.5 : 1.0
     rt___@apechtold___: = True              D66 : PVV    =     50.6 : 1.0
           rt___@d66___: = True              D66 : CDA    =     50.3 : 1.0
     rt___@mirjam152___: = True              PVV : CDA    =     49.7 : 1.0
               ______± = True              PVV : GroenL =     49.4 : 1.0
    :___.___@sybrandbuma = True              CDA : GroenL =     48.6 : 1.0
       rt___@bpschut___: = True              PVV : GroenL =     47.4 : 1.0
   rt___@sybrandbuma___: = True              CDA : GroenL =     47.3 : 1.0
   rt___@samvanrooy1___: = True              PVV : CDA    =     45.4 : 1.0
  rt___@prisonplanet___: = True              PVV : CDA    =     45.4 : 1.0
              ______³ð = True              PVV : GroenL =     45.3 : 1.0
              ___³ð___ = True              PVV : GroenL =     45.3 : 1.0
      rt___@umarebru___: = True              PVV : GroenL =     44.7 : 1.0
              ³ð______ = True              PVV : GroenL =     44.7 : 1.0
     @cdavandaag___:___. = True              CDA : D66    =     44.2 : 1.0
   rt___@monakeijzer___: = True              CDA : D66    =     44.2 : 1.0
   rt___@peteranshof___: = True              PVV : CDA    =     44.0 : 1.0
       rt___@keesvee___: = True              D66 : GroenL =     43.5 : 1.0
         land___dat___we = True              CDA : GroenL =     41.0 : 1.0
              ¼ï___¸___ = True              PVV : GroenL =     40.6 : 1.0
              ___¼ï___¸ = True              PVV : GroenL =     40.6 : 1.0
         dat___we___door = True              CDA : PVV    =     40.1 : 1.0
      we___door___willen = True              CDA : PVV    =     39.4 : 1.0
   rt___@v_of_europe___: = True              PVV : CDA    =     39.3 : 1.0
              â______¼ï = True              PVV : GroenL =     37.8 : 1.0
               ð______ = True              PVV : GroenL =     37.8 : 1.0
       voor___een___land = True              CDA : D66    =     37.4 : 1.0
               ______ð = True              PVV : GroenL =     36.5 : 1.0
               â___¬___ = True              D66 : CDA    =     36.3 : 1.0
   door___willen___geven = True              CDA : PVV    =     36.3 : 1.0
rt___@drgertjanmulder___: = True              PVV : CDA    =     34.0 : 1.0
    .___@sybrandbuma___: = True              CDA : PVV    =     33.8 : 1.0
               ___â___ = True              PVV : GroenL =     33.7 : 1.0
     rt___@ananninga___: = True              PVV : GroenL =     33.7 : 1.0
             ã___¡___lle = True              D66 : CDA    =     33.5 : 1.0
 rt___@svanweyenberg___: = True              D66 : GroenL =     33.3 : 1.0
         rt___@tponl___: = True              PVV : GroenL =     33.1 : 1.0
   rt___@jesseklaver___: = True           GroenL : CDA    =     32.5 : 1.0
            in___nl___en = True              PVV : GroenL =     32.4 : 1.0
        rt___@ovcned___: = True              PVV : D66    =     31.5 : 1.0
             :___cda___: = True              CDA : GroenL =     31.4 : 1.0



Calculating most informative features of friends ...
Most Informative Features
              groenlinks = True           GroenL : PVV    =     42.4 : 1.0
                umarebru = True              PVV : GroenL =     33.1 : 1.0
              cdavandaag = True              CDA : GroenL =     28.4 : 1.0
                THiddema = True              PVV : CDA    =     26.1 : 1.0
            harmbeertema = True              PVV : CDA    =     24.0 : 1.0
               ANanninga = True              PVV : GroenL =     23.5 : 1.0
            fvdemocratie = True              PVV : GroenL =     23.5 : 1.0
         DrGertJanMulder = True              PVV : D66    =     22.9 : 1.0
         lilianhelderpvv = True              PVV : GroenL =     22.2 : 1.0
           frenkie4allll = True              PVV : CDA    =     21.8 : 1.0
                rjklever = True              PVV : CDA    =     21.8 : 1.0
           lindavoortman = True           GroenL : PVV    =     21.2 : 1.0
           Vera_Bergkamp = True              D66 : PVV    =     21.1 : 1.0
           svanweyenberg = True              D66 : GroenL =     20.9 : 1.0
          michieldijkman = True              CDA : GroenL =     20.6 : 1.0
            PrisonPlanet = True              PVV : CDA    =     20.4 : 1.0
                 KeesVee = True              D66 : GroenL =     20.2 : 1.0
          Percolator_HNJ = True              PVV : D66    =     20.2 : 1.0
           GroenLiesbeth = True           GroenL : PVV    =     19.9 : 1.0
             SamvanRooy1 = True              PVV : CDA    =     19.7 : 1.0
           FleurAgemaPVV = True              PVV : GroenL =     19.4 : 1.0
         tRadicaleMidden = True              D66 : CDA    =     19.1 : 1.0
            PeterSweden7 = True              PVV : CDA    =     19.0 : 1.0
                hulswood = True              PVV : D66    =     18.9 : 1.0
          JoostNiemoller = True              PVV : CDA    =     18.7 : 1.0
           Derksen_Gelul = True              PVV : CDA    =     18.2 : 1.0
          GraafdeMachiel = True              PVV : CDA    =     18.2 : 1.0
             DDStandaard = True              PVV : GroenL =     18.1 : 1.0
          AgnesMulderCDA = True              CDA : D66    =     17.6 : 1.0
          MvanToorenburg = True              CDA : GroenL =     17.5 : 1.0
              Martijncda = True              CDA : GroenL =     17.5 : 1.0
           SophieintVeld = True              D66 : CDA    =     16.9 : 1.0
                henkbres = True              PVV : D66    =     16.9 : 1.0
            PieterHeerma = True              CDA : GroenL =     16.8 : 1.0
                 bpschut = True              PVV : CDA    =     16.8 : 1.0
              Maikel_PVV = True              PVV : CDA    =     16.8 : 1.0
               EuWaanzin = True              PVV : CDA    =     16.8 : 1.0
             JoyceKardol = True              PVV : GroenL =     16.7 : 1.0
            VickyMaeijer = True              PVV : D66    =     16.2 : 1.0
               YidArmyNL = True              PVV : D66    =     16.2 : 1.0
         Paul_van_Meenen = True              D66 : CDA    =     16.2 : 1.0
         StuivenbergInfo = True              PVV : CDA    =     16.1 : 1.0
              HitradioNL = True              PVV : CDA    =     16.1 : 1.0
            Nigel_Farage = True              PVV : GroenL =     16.0 : 1.0
             ruthpeetoom = True              CDA : D66    =     15.7 : 1.0
            HarryPater62 = True              PVV : D66    =     15.6 : 1.0
         PegidaNederland = True              PVV : D66    =     15.6 : 1.0
            jpbalkenende = True              CDA : GroenL =     15.6 : 1.0
         RidderDionGraus = True              PVV : CDA    =     15.4 : 1.0
            RaymondKnops = True              CDA : D66    =     15.1 : 1.0



Calculating most informative features of hashtags ...
Most Informative Features
                #stempvv = True              PVV : CDA    =     97.7 : 1.0
                #stemd66 = True              D66 : GroenL =     96.3 : 1.0
              #ikstemcda = True              CDA : D66    =     87.4 : 1.0
    #stemvoorverandering = True           GroenL : CDA    =     59.8 : 1.0
             #d66bustour = True              D66 : CDA    =     42.8 : 1.0
           #grenzendicht = True              PVV : CDA    =     36.1 : 1.0
    #nederlandweervanons = True              PVV : CDA    =     33.7 : 1.0
                 #verzet = True              PVV : D66    =     32.8 : 1.0
                  #nexit = True              PVV : CDA    =     32.4 : 1.0
    #tijdvoorverandering = True           GroenL : CDA    =     31.1 : 1.0
                #wietwet = True              D66 : PVV    =     30.5 : 1.0
                    #fvd = True              PVV : CDA    =     29.8 : 1.0
#nietschreeuwenmaarpraten = True           GroenL : CDA    =     29.7 : 1.0
                    #mkb = True              CDA : PVV    =     29.5 : 1.0
                  #ovc16 = True              PVV : D66    =     29.5 : 1.0
              #stopislam = True              PVV : CDA    =     28.3 : 1.0
                   #maga = True              PVV : CDA    =     26.8 : 1.0
           #geertwilders = True              PVV : D66    =     26.2 : 1.0
                   #stem = True              PVV : CDA    =     26.1 : 1.0
              #onderwijs = True              D66 : PVV    =     25.8 : 1.0
                 #turken = True              PVV : GroenL =     24.2 : 1.0
                     #gl = True              PVV : CDA    =     23.8 : 1.0
               #breaking = True              PVV : GroenL =     22.8 : 1.0
                 #nldoet = True              CDA : PVV    =     20.8 : 1.0
               #demeetup = True           GroenL : CDA    =     20.6 : 1.0
                    #vnl = True              PVV : GroenL =     20.1 : 1.0
                  #stemâ = True           GroenL : CDA    =     19.9 : 1.0
                  #lepen = True              PVV : GroenL =     18.7 : 1.0
                 #klaver = True              PVV : CDA    =     18.7 : 1.0
                   #denk = True              PVV : GroenL =     18.3 : 1.0
            #leenstelsel = True              CDA : GroenL =     18.1 : 1.0
             #stemwijzer = True              CDA : GroenL =     17.5 : 1.0
                  #bumor = True              CDA : D66    =     17.0 : 1.0
                  #carrã = True              CDA : PVV    =     17.0 : 1.0
                #pvv2017 = True              PVV : D66    =     16.5 : 1.0
                 #france = True              PVV : CDA    =     16.1 : 1.0
             #verkiezing = True              PVV : GroenL =     16.0 : 1.0
                 #merkel = True              PVV : GroenL =     15.7 : 1.0
             #marine2017 = True              PVV : D66    =     15.6 : 1.0
             #groenlinks = True           GroenL : CDA    =     15.5 : 1.0
               #vacature = True              CDA : PVV    =     15.2 : 1.0
             #ondernemer = True              CDA : GroenL =     14.9 : 1.0
                 #turkey = True              PVV : GroenL =     14.8 : 1.0
                   #kuzu = True              PVV : GroenL =     14.7 : 1.0
                #germany = True              PVV : GroenL =     14.0 : 1.0
              #turkenrel = True              PVV : CDA    =     14.0 : 1.0
                     #eu = True              PVV : GroenL =     13.6 : 1.0
                   #zorg = True              CDA : PVV    =     13.6 : 1.0
                 #moslim = True              PVV : D66    =     13.6 : 1.0
                 #israel = True              PVV : GroenL =     13.3 : 1.0