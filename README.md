# README #
*
### What is this repository for? ###
This repository contains all documents which I have used for my bachelor thesis for classifying Twiter users based on word use, friends and hashtags.

	Directory 'output' contains three sub-directories:
		output_process_data: contains the output files of terminals of process_data.py
		output_classify_data_dev: contains the output files of terminals of classify_data.py, ran on the development set
		output_classify_data: contains the output files of terminals of classify_data.py, ran on the total set

	Directory 'classification' contains all .py files and sub-directories with other files used for the entire classifying process:
		pickle_files: contains the pickle files created with process_data.py and used in classify_data.py
		txt_files: all .txt files created and used by different .py programs
		unfiltered_tweets_files: 1.4GB of files which have were the output of downloading tweets and uer names out of the RUG Twitter Corpus
	

### How do I get set up? ###
First download the repository with:	 git clone https://GabayTomer@bitbucket.org/GabayTomer/bachelor_thesis.git

You can then analyze the files located in different directories.

Overview of .py programs:
	
	cd classification
	
	python collect_data.py:
		input:
			1.4GB of .txt files from unfiltered_tweet_files (username	tweet)
			party_keywords.txt (Abbrevation party name|keyword,keyword,keyword...)
		output:
			tweets_with_stem.txt (username	tweet)
			party_committing_tweets.txt (username	party_committing_tweet	party)
			data_raw.p (dictionary with key = user, value = [[tweets],[friends],party])
	
	python3 reduce_party_committing_data.py: 
		input:
			party_committing_tweets.txt (username	party_committing_tweet	party)
		output:
			4000_party_committing_tweets.txt (username	party_committing_tweet party). 1000 users per party
	
	python3 process_data.py:
		input:
			4000_party_committing_tweets.txt (username	party_committing_tweet 	party)
			data_raw.p (dictionary with key = user, value = [[tweets],[friends],party])
		output:
			data_all.p / data_personal.p / data_RT.p ( [(X,'unigrams'),(Xbi,'bigrams'),(Xtri,'trigrams'),(Xfriends,'friends'),(Xhashtags,'hashtags')],Y,[(uni_feats,'unigrams'),(bi_feats,'bigrams'),(tri_feats,' trigrams'),(friends_feats,'friends'),(hashtags_feats,'hashtags')]
	
	python3 classify_data.py all/personal/retweets dev/total:
		input:
			data_all.p / data_personal.p / data_RT.p ( [(X,'unigrams'),(Xbi,'bigrams'),(Xtri,'trigrams'),(Xfriends,'friends'),(Xhashtags,'hashtags')],Y,[(uni_feats,'unigrams'),(bi_feats,'bigrams'),(tri_feats,' trigrams'),(friends_feats,'friends'),(hashtags_feats,'hashtags')]
		output:
			classification results in terminal
	
	python3 annotate_data.py
		input:
			4000_party_committing_tweets.txt (username	party_committing_tweet	party)
		output:
			pre_annotated_tweets.txt (party_committing_tweet	party). 100 random samples of the 4000
			annotated_tweets.txt (party_committing_tweet	party	Y/N). Y/N stand for Yes correct label or No incorrect label